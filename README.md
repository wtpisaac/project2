# Project 2 | Networking
This is the second reference project for the Slugbotics Qt Projects, dealing
with networking.

Please note: This reference project is NOT complete. I'll update this
when we have the project complete (if this is left in here when I release
the project document, please yell at me).

All code should be considered public domain under the CC0 license,
available here:
https://creativecommons.org/publicdomain/zero/1.0/legalcode
