#ifndef MESSAGES_H
#define MESSAGES_H

#include <QObject>
#include <QWidget>
#include <QString>
#include <QStringListModel>

class Messages : public QStringListModel
{
    Q_OBJECT
public:
    Messages();
    void append(QString message);
};

#endif // MESSAGES_H
