#include "chat.h"

Chat::Chat(quint16 port, QObject *parent) : QObject(parent)
{
    // configure server
    this->server = new QTcpServer(this);
    if(!server->listen(QHostAddress::AnyIPv4, port)) {
        qDebug() << "Problem encountered with initializing TCP server.";
        // server->close();
        exit(-1);
    }

    qDebug() << server->serverAddress().toString() << server->serverPort();
    connect(server, &QTcpServer::newConnection, this, &Chat::didRecieveClientConnection);

    // in.setVersion(QDataStream::Qt_5_12);
}

void Chat::stopServer()
{
    server->close();    // TODO investigate effects of this
}

void Chat::connectToClient(QHostAddress address, quint64 port)
{
   // disconnect(server, &QTcpServer::newConnection, this, &Chat::didRecieveClientConnection);
   stopServer();
   this->peerSocket = new QTcpSocket();

   // setup connections
   connect(peerSocket, &QTcpSocket::connected, this, &Chat::didRecieveClientConnection);

   qDebug() << "Attempting outgoing connection";
   peerSocket->connectToHost(address, port);
}

void Chat::didRecieveClientConnection() {
    qDebug() << "did recieve client conn";
    // prevent further connections, for simplicity.
    //disconnect(server, &QTcpServer::newConnection, this, &Chat::didRecieveClientConnection);

    // set peer socket only if non-null
    if(peerSocket == nullptr) peerSocket = this->server->nextPendingConnection();

    // stop the server
    stopServer();

    connect(this->peerSocket, &QTcpSocket::readyRead, this, &Chat::readMessage);

    // configure data stream
    // in.setDevice(this->peerSocket);

    //sendingEnabled = true;
    emit(didConnect());
}

void Chat::readMessage() {
    // begin reading through
    // use the vector in our class
    while(peerSocket->bytesAvailable()) {
        // read and analyze the byte being sent
        char c;
        peerSocket->getChar(&c);

        if(c == '\n')
        {
            // we've reached a message break

            // ensure buffer is null terminated (prevent memory badness)
            buffer.push_back('\0');

            // flush the buffer out as a message
            char* message_char_arr = buffer.data();

            // emit our message
            emit messageRecieved(QString(message_char_arr), this->peerSocket->peerAddress().toString());

            // clear the buffer
            buffer.clear();
        }
        else
        {
            buffer.push_back(c);
        }

    }
}

void Chat::sendMessage(QString message)
{
    if(peerSocket != nullptr) {
//        QByteArray block;
//        QDataStream out(&block, QIODevice::WriteOnly);
//        out.setVersion(QDataStream::Qt_5_12);

//        out << message;

        // prepare message for sending by appending return
        message.append("\n");

        // create a char* array from the data for sending over the network
        const QChar* data = message.constData();
        const int messageDataSize = message.size();

        char* messageCharArr = (char*)calloc(messageDataSize, sizeof(char));

        for(int i=0; i<messageDataSize; i++) { // for all characters
            messageCharArr[i] = data[i].toLatin1(); // convert to ASCII
        }

        peerSocket->write(messageCharArr, messageDataSize);

        // deallocate array (to avoid memory leak)
        free(messageCharArr);
    }
}
