#ifndef CHATSERVER_H
#define CHATSERVER_H

#include <QObject>
#include <QHostAddress>
#include <QTcpSocket>
#include <QTcpServer>
#include <QByteArray>
#include <QDataStream>
#include <QTextStream>
#include <vector>
#include <string.h>

class Chat : public QObject
{
    Q_OBJECT
public:
    explicit Chat(quint16 port, QObject *parent = nullptr);
    void connectToClient(QHostAddress address, quint64 port);
public slots:
    void sendMessage(QString message);
private:
    void stopServer();
    QTcpServer* server = nullptr;
    QTcpSocket* peerSocket = nullptr;
    // QDataStream in;
    bool sendingEnabled = false;
    std::vector<char> buffer;
private slots:
    void didRecieveClientConnection();
    void readMessage();
signals:
    void messageRecieved(QString message, QString peerIdentity);
    void serverError(QString errorMessage);
    void didConnect();

};

#endif // CHATSERVER_H
