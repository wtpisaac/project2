#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "messages.h"
#include "connecttodialog.h"

#include <QListView>
#include <QStringListModel>
#include <QPushButton>
#include <QLineEdit>
#include <QInputDialog>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->messages = new Messages();
    this->ui->messagesListView->setModel((QStringListModel*)this->messages);

    connect(this->ui->sendButton, &QPushButton::pressed, this, &MainWindow::append);
    connect(this->ui->messageEdit, &QLineEdit::returnPressed, this, &MainWindow::append);
    connect(this->ui->connectButton, &QPushButton::pressed, this, &MainWindow::openConnectionDialog);

    bool ok = false;
    while(!ok) {
        int portChoice = QInputDialog::getInt(this, "Select hosting port", "What port do you want to host your chat on? (Cancel to quit)", 0, 0, 65535, 1, &ok);
        if(ok) {
            chat = new Chat(portChoice, this);
        } else {
            exit(0);
        }
    }
    connect(this->chat, &Chat::messageRecieved, this, &MainWindow::appendNetworkMessage);
    connect(chat, &Chat::didConnect, this, &MainWindow::disableConnectionButton);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::append() {
    // grab trimmed message from send box
    QString messageString = this->ui->messageEdit->text().trimmed();

    // check that the string isn't empty
    if(messageString.size() == 0)
    {
        return;
    }
    // clear box
    this->ui->messageEdit->clear();
    // append to the model
    this->messages->append(messageString);
    // actually send it (move this / refactor)
    chat->sendMessage(messageString);
}

void MainWindow::appendNetworkMessage(QString message, QString peerIdentity) {
    this->messages->append(peerIdentity + ": " + message);
}

void MainWindow::openConnectionDialog()
{
    ConnectToDialog* cDialog = new ConnectToDialog(this);
    connect(cDialog, &ConnectToDialog::connectionIntent, chat, &Chat::connectToClient);
    cDialog->show();
}

void MainWindow::disableConnectionButton()
{
    ui->connectButton->setEnabled(false);
}
