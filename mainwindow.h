#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "messages.h"
#include "chat.h"
#include "connecttodialog.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
public slots:
    void append();
    void appendNetworkMessage(QString message, QString peerIdentity);
    void disableConnectionButton();
private:
    Ui::MainWindow *ui;
    Messages* messages;
    Chat* chat;
    void openConnectionDialog();
};
#endif // MAINWINDOW_H
