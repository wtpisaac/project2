#ifndef CONNECTTODIALOG_H
#define CONNECTTODIALOG_H

#include <QDialog>
#include <QHostAddress>

namespace Ui {
class ConnectToDialog;
}

class ConnectToDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConnectToDialog(QWidget *parent = nullptr);
    ~ConnectToDialog();

private:
    Ui::ConnectToDialog *ui;
private slots:
    void userDidAccept();
signals:
    void connectionIntent(QHostAddress address, quint64 port);
};

#endif // CONNECTTODIALOG_H
