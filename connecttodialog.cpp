#include "connecttodialog.h"
#include "ui_connecttodialog.h"

ConnectToDialog::ConnectToDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConnectToDialog)
{
    ui->setupUi(this);
    connect(this, &QDialog::accepted, this, &ConnectToDialog::userDidAccept);
}

ConnectToDialog::~ConnectToDialog()
{
    delete ui;
}

void ConnectToDialog::userDidAccept()
{
    QHostAddress address(this->ui->ipEdit->text().trimmed());
    quint64 port = (quint64)this->ui->portEdit->text().trimmed().toUInt();

    if(address.protocol() == QAbstractSocket::IPv4Protocol) { // if valid ip
        emit connectionIntent(address, port);
        qDebug() << "Emitted signal";
    } else qDebug() << "mistakes were made";
}
